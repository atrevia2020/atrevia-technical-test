if [[ $@ == *--with-hosts* ]]
then
  echo '127.0.0.1 api.test-monorepo.docker' | sudo tee -a /etc/hosts
fi

docker-compose -f docker/docker-compose.yml -p test-monorepo build
docker-compose -f docker/docker-compose.yml -p test-monorepo up -d
docker-compose -f docker/docker-compose.yml -p test-monorepo exec php php /usr/local/bin/composer install