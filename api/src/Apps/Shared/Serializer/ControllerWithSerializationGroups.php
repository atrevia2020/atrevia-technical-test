<?php


namespace App\Apps\Shared\Serializer;


interface ControllerWithSerializationGroups
{
    public function getSerializationGroups(): array;
}