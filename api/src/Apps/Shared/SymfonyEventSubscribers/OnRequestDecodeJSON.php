<?php


namespace App\Apps\Shared\SymfonyEventSubscribers;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class OnRequestDecodeJSON implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): iterable
    {
        return [
            KernelEvents::REQUEST => 'onRequest'
        ];
    }

    public function onRequest(RequestEvent $event): void
    {

        $request = $event->getRequest();

        if (str_starts_with($request->headers->get('Content-Type'), 'application/json')) {
            $data = json_decode($request->getContent(), true);
            $request->request->replace(is_array($data) ? $data : []);
        }

        if (str_starts_with($request->headers->get('Content-Type'), 'multipart/form-data')) {
            // Convierte "false" y "true" a false y true
            $data = array_map(static function ($param) {
                if ($param !== 'true' &&  $param !== 'false'){
                    return $param;
                }
                return $param === 'true';
            }, $request->request->all());

            $request->request->replace(is_array($data) ? $data : []);
        }
    }
}