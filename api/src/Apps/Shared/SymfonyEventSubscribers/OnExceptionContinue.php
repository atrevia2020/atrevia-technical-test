<?php


namespace App\Apps\Shared\SymfonyEventSubscribers;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Messenger\Exception\HandlerFailedException;

class OnExceptionContinue implements EventSubscriberInterface
{
    public function __construct()
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => 'onException'
        ];
    }

    public function onException(ExceptionEvent $event) {
        $throwable = $event->getThrowable();

        if ($throwable instanceof HandlerFailedException) {
            $throwable = $throwable->getPrevious();
        }

        $message = $throwable->getMessage();

        if ($throwable instanceof HttpException) {
            $event->setResponse(new JsonResponse(['error' => $message], $throwable->getStatusCode()));
        } else {
            $event->setResponse(new JsonResponse([
                'error' => $message,
                'line' => $throwable->getLine(),
                'file' => $throwable->getFile(),
                'trace' => $throwable->getTrace()
            ], Response::HTTP_INTERNAL_SERVER_ERROR));
        }
    }
}
