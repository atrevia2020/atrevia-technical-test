<?php


namespace App\Apps\Shared\SymfonyEventSubscribers;

use App\Apps\Shared\Serializer\ControllerWithSerializationGroups;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class OnViewSerialize implements EventSubscriberInterface
{
    private $controller;
    public function __construct(
        private SerializerInterface $serializer
    )
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => 'onView',
            KernelEvents::CONTROLLER => 'onController'
        ];
    }

    public function onController(ControllerEvent $event): void
    {
        $this->controller = $event->getController();
    }

    public function onView(ViewEvent $event): void
    {
        $result = $event->getControllerResult();

        if ($result === null) {
            $event->setResponse(new JsonResponse());
            return;
        }

        $context = SerializationContext::create();
        $context->enableMaxDepthChecks();
        $context->setSerializeNull(true);

        if ($this->controller instanceof ControllerWithSerializationGroups) {
            $context->setGroups($this->controller->getSerializationGroups());
        }

        $response = new Response(
            $this->serializer->serialize($result, 'json', $context),
            Response::HTTP_OK,
            [
                'Content-Type' => 'application/json'
            ]
        );
        $event->setResponse($response);
    }
}