<?php


namespace App\Apps\Backoffice\DiscountCoupon;


use App\Modules\DiscountCoupon\Application\Queries\DiscountCouponByCodeQuery;
use App\Modules\Shared\Application\QueryBus;
use App\Modules\Shared\Infrastructure\SymfonyMessenger\SymfonyMessengerQueryBus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class QueryDiscountCouponByCode extends AbstractController
{
    public function __construct(
        private QueryBus $queryBus,
    )
    {
    }

    #[Route('/coupons/code/{code}', name: 'QueryDiscountCouponByCode', methods: ['GET'])]
    public function __invoke(Request $request)
    {
        return $this->queryBus->query(new DiscountCouponByCodeQuery(
            $request->get('code')
        ));
    }
}