<?php


namespace App\Apps\Backoffice\DiscountCoupon;


use App\Modules\DiscountCoupon\Application\Commands\CreateDiscountCouponCommand;
use App\Modules\DiscountCoupon\Application\Commands\DeactivateDiscountCouponCommand;
use App\Modules\DiscountCoupon\Application\Commands\RedeemDiscountCouponCommand;
use App\Modules\Shared\Infrastructure\SymfonyMessenger\SymfonyMessengerCommandBus;
use App\Modules\Shared\Infrastructure\RamseyUuidGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DoDeactivateDiscountCoupon extends AbstractController
{
    public function __construct(
        private SymfonyMessengerCommandBus $commandBus
    )
    {
    }

    #[Route('/coupons/{uuid}/deactivate', name: 'DoDeactivateDiscountCoupon', methods: ['POST'])]
    public function __invoke(Request $request)
    {
        return $this->commandBus->handle(new DeactivateDiscountCouponCommand(
            $request->get('uuid')
        ));
    }
}
