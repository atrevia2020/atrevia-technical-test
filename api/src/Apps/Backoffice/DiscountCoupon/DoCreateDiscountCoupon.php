<?php


namespace App\Apps\Backoffice\DiscountCoupon;


use App\Modules\DiscountCoupon\Application\Commands\CreateDiscountCouponCommand;
use App\Modules\Shared\Infrastructure\SymfonyMessenger\SymfonyMessengerCommandBus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DoCreateDiscountCoupon extends AbstractController
{
    public function __construct(
        private SymfonyMessengerCommandBus $commandBus
    )
    {
    }

    #[Route('/coupons/{uuid}', name: 'DoCreateDiscountCoupon', methods: ['POST'])]
    public function __invoke(Request $request)
    {
        return $this->commandBus->handle(new CreateDiscountCouponCommand(
            $request->get('uuid'),
            $request->request->get('code')
        ));
    }
}
