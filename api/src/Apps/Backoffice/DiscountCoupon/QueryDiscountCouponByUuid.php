<?php


namespace App\Apps\Backoffice\DiscountCoupon;


use App\Modules\DiscountCoupon\Application\Queries\DiscountCouponByUuidQuery;
use App\Modules\Shared\Application\QueryBus;
use App\Modules\Shared\Infrastructure\SymfonyMessenger\SymfonyMessengerQueryBus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

class QueryDiscountCouponByUuid extends AbstractController
{
    public function __construct(
        private QueryBus $queryBus
    )
    {
    }

    #[Route('/coupons/{uuid}', name: 'QueryDiscountCouponByUuid', methods: ['GET'])]
    public function __invoke(Request $request)
    {
        return $this->queryBus->query(new DiscountCouponByUuidQuery($request->get('uuid')));
    }
}