<?php


namespace App\Apps\Backoffice\DiscountCoupon;


use App\Modules\DiscountCoupon\Application\Commands\CreateDiscountCouponCommand;
use App\Modules\DiscountCoupon\Application\Commands\RedeemDiscountCouponCommand;
use App\Modules\Shared\Infrastructure\SymfonyMessenger\SymfonyMessengerCommandBus;
use App\Modules\Shared\Infrastructure\RamseyUuidGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DoRedeemDiscountCoupon extends AbstractController
{
    public function __construct(
        private SymfonyMessengerCommandBus $commandBus
    )
    {
    }

    #[Route('/coupons/{uuid}/redeem', name: 'DoRedeemDiscountCoupon', methods: ['POST'])]
    public function __invoke(Request $request)
    {
        return $this->commandBus->handle(new RedeemDiscountCouponCommand(
            $request->get('uuid'),
            $request->request->get('redeemerEmail')
        ));
    }
}
