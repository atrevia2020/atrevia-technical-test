<?php

namespace App\Modules\DiscountCoupon\Domain;


use App\Modules\Shared\Domain\AggregateRoot;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @ORM\Entity(repositoryClass="App\Modules\DiscountCoupon\Infrastructure\DiscountCouponDoctrineRepositoryDoctrine")
 * @ORM\Table(name="discount_coupon")
 * @ORM\HasLifecycleCallbacks()
 */
final class DiscountCoupon extends AggregateRoot
{
    /**
     * @ORM\Column(name="name", type="string")
     */
    private string $code;

    /**
     * @ORM\Column(name="active", type="boolean")
     */
    private bool $active = true;

    /**
     * @ORM\Column(name="redeemer_email", type="string", nullable=true)
     */
    private ?string $redeemerEmail = null;


    public static function register(
        string $uuid,
        string $code
    ): self
    {
        $discountCoupon = new self($uuid);

        $discountCoupon->code = $code;
        $discountCoupon->active = true;

        $discountCoupon->record(new DiscountCouponCreatedEvent($discountCoupon->uuid()));

        return $discountCoupon;
    }

    public function deactivate(): void
    {
        if (!$this->active) {
            throw new HttpException(Response::HTTP_CONFLICT, 'The code has been previously deactivated');
        }

        $this->active = false;

        $this->record(new DiscountCouponDeactivatedEvent($this->uuid()));
    }

    public function rename(
        string $code,
    ): void
    {
        $this->code = $code;

        $this->record(new DiscountCouponRenamedEvent($this->uuid()));
    }

    public function redeem(string $redeemerEmail)
    {
        if ($this->hasBeenRedeemed() || $this->hasBeenDeactivated()) {
            throw new HttpException(Response::HTTP_CONFLICT, 'The code has been previously redeemed or deactivated');
        }

        if (!filter_var($redeemerEmail, FILTER_VALIDATE_EMAIL)){
            throw new HttpException(Response::HTTP_BAD_REQUEST, 'Invalid redeemer email');
        }

        $this->active = false;

        $this->redeemerEmail = $redeemerEmail;

        $this->record(new DiscountCouponRedeemedEvent($this->uuid()));
    }

    public function hasBeenRedeemed(): bool
    {
        return $this->redeemerEmail !== null;
    }

    public function hasBeenDeactivated(): bool
    {
        return $this->active === false;
    }
}
