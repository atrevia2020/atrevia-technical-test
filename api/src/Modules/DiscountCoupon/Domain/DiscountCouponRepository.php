<?php

namespace App\Modules\DiscountCoupon\Domain;


/**
 * @method DiscountCoupon|null find($id, $lockMode = null, $lockVersion = null)
 * @method DiscountCoupon|null findOneBy(array $criteria, array $orderBy = null)
 * @method DiscountCoupon[]    findAll()
 * @method DiscountCoupon[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method DiscountCoupon      findOrFail($id)
 */
interface DiscountCouponRepository
{
    public function exists(string $code): bool;

    public function ifExistsFail(string $code): void;

    public function findOneByCodeOrFail(string $discountCoupon): DiscountCoupon;
}