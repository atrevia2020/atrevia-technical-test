<?php

namespace App\Modules\DiscountCoupon\Infrastructure;

use App\Modules\DiscountCoupon\Domain\DiscountCoupon;
use App\Modules\DiscountCoupon\Domain\DiscountCouponRepository;
use App\Modules\Shared\Infrastructure\Doctrine\DoctrineAbstractRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @method DiscountCoupon|null find($id, $lockMode = null, $lockVersion = null)
 * @method DiscountCoupon|null findOneBy(array $criteria, array $orderBy = null)
 * @method DiscountCoupon[]    findAll()
 * @method DiscountCoupon[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method DiscountCoupon      findOrFail($id)
 */
final class DiscountCouponDoctrineRepositoryDoctrine extends DoctrineAbstractRepository implements DiscountCouponRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DiscountCoupon::class);
    }

    public function exists(string $code): bool
    {
        $result = $this->createQueryBuilder('u')
            ->andWhere('UPPER(u.code) = :code')
            ->setParameter('code', strtoupper($code))
            ->getQuery()
            ->getResult();

        return !($result === null || count($result) === 0);
    }

    public function ifExistsFail(string $code): void
    {
        if ($this->exists($code)) {
            throw new HttpException(409, "{$this->_entityName} with code '$code' already exists");
        }
    }

    public function findOneByCodeOrFail(string $discountCoupon): DiscountCoupon
    {
        $discountCoupon = $this->findOneBy(['code' => $discountCoupon]);

        if ($discountCoupon === null) {
            throw new HttpException(404, "{$this->_entityName} with code '$discountCoupon' does not exists");
        }

        return $discountCoupon;
    }

}
