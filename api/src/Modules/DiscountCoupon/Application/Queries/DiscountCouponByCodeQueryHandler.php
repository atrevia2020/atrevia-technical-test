<?php


namespace App\Modules\DiscountCoupon\Application\Queries;


use App\Modules\DiscountCoupon\Domain\DiscountCouponRepository;
use App\Modules\Shared\Application\QueryHandler;

final class DiscountCouponByCodeQueryHandler extends QueryHandler
{
    public function __construct(
        private DiscountCouponRepository $discountCouponRepository
    )
    {
    }

    public function __invoke(DiscountCouponByCodeQuery $query)
    {
        return $this->discountCouponRepository->findOneByCodeOrFail($query->code);
    }
}