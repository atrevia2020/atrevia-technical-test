<?php


namespace App\Modules\DiscountCoupon\Application\Queries;

use App\Modules\DiscountCoupon\Domain\DiscountCouponRepository;
use App\Modules\Shared\Application\QueryHandler;

final class DiscountCouponByUuidQueryHandler extends QueryHandler
{
    public function __construct(
        private DiscountCouponRepository $discountCouponRepository
    )
    {
    }

    public function __invoke(DiscountCouponByUuidQuery $query)
    {
        return $this->discountCouponRepository->findOrFail($query->uuid);
    }
}