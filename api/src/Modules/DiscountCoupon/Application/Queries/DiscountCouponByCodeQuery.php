<?php

namespace App\Modules\DiscountCoupon\Application\Queries;

use App\Modules\Shared\Application\Query;

final class DiscountCouponByCodeQuery implements Query
{
    public function __construct(
        public readonly string $code,
    )
    {
    }
}