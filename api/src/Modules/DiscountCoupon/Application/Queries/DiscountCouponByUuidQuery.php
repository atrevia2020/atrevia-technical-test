<?php

namespace App\Modules\DiscountCoupon\Application\Queries;

use App\Modules\Shared\Application\Query;

final class DiscountCouponByUuidQuery implements Query
{
    public function __construct(
        public readonly string $uuid,
    )
    {
    }
}