<?php


namespace App\Modules\DiscountCoupon\Application\Commands;

use App\Modules\DiscountCoupon\Domain\DiscountCouponRepository;
use App\Modules\Shared\Application\CommandHandler;
use App\Modules\Shared\Application\EventBus;

final class RenameDiscountCodeCommandHandler extends CommandHandler
{
    public function __construct(
        private DiscountCouponRepository $discountCouponRepository,
        private EventBus $eventBus
    )
    {
    }

    public function __invoke(RenameDiscountCouponCommand $command)
    {
        $discountCoupon = $this->discountCouponRepository->findOrFail($command->uuid);

        $discountCoupon->rename($command->code);

        $this->eventBus->dispatch(...$discountCoupon->getDomainEvents());
    }
}
