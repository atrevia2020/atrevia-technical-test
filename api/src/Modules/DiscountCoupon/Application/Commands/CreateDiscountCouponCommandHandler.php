<?php


namespace App\Modules\DiscountCoupon\Application\Commands;

use App\Modules\DiscountCoupon\Domain\DiscountCoupon;
use App\Modules\DiscountCoupon\Domain\DiscountCouponRepository;
use App\Modules\Shared\Application\CommandHandler;
use App\Modules\Shared\Application\EventBus;

final class CreateDiscountCouponCommandHandler extends CommandHandler
{
    public function __construct(
        private DiscountCouponRepository $discountCouponRepository,
        private EventBus $eventBus
    )
    {
    }

    public function __invoke(CreateDiscountCouponCommand $command)
    {
        $this->discountCouponRepository->ifExistsFail($command->code);

        $discountCoupon = DiscountCoupon::register(
            $command->uuid,
            $command->code
        );

        $this->discountCouponRepository->persist($discountCoupon);

        $this->eventBus->dispatch(...$discountCoupon->getDomainEvents());
    }
}
