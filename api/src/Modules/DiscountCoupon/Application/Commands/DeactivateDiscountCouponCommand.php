<?php

namespace App\Modules\DiscountCoupon\Application\Commands;

use App\Modules\Shared\Application\Command;

final class DeactivateDiscountCouponCommand implements Command
{
    public function __construct(
        public readonly string $uuid
    )
    {
    }
}
