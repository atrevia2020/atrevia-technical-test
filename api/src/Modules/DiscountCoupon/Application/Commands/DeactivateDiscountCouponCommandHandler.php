<?php


namespace App\Modules\DiscountCoupon\Application\Commands;

use App\Modules\DiscountCoupon\Domain\DiscountCouponRepository;
use App\Modules\Shared\Application\CommandHandler;
use App\Modules\Shared\Application\EventBus;

final class DeactivateDiscountCouponCommandHandler extends CommandHandler
{
    public function __construct(
        private DiscountCouponRepository $discountCouponRepository,
        private EventBus $eventBus
    )
    {
    }

    public function __invoke(DeactivateDiscountCouponCommand $command)
    {
        $discountCoupon = $this->discountCouponRepository->findOrFail($command->uuid);

        $discountCoupon->deactivate();

        $this->eventBus->dispatch(...$discountCoupon->getDomainEvents());
    }
}
