<?php

namespace App\Modules\DiscountCoupon\Application\Commands;

use App\Modules\Shared\Application\Command;

final class RedeemDiscountCouponCommand implements Command
{
    public function __construct(
        public readonly string $uuid,
        public readonly string $redeemerEmail
    )
    {
    }
}
