<?php

namespace App\Modules\Shared\Application;

use App\Modules\Shared\Domain\Event;

interface EventBus
{
    public function getLastEventDispatched(): Event;

    public function dispatch(Event ...$events): void;

    public function getEventsDispatched(): array;
}