<?php

namespace App\Modules\Shared\Application;

interface EmailSender
{
    public function send(string $subject, string $message, array $emails): void;
}
