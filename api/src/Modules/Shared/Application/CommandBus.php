<?php

namespace App\Modules\Shared\Application;

interface CommandBus
{
    public function handle(Command $command);
}