<?php

namespace App\Modules\Shared\Application;

interface QueryBus
{
    public function query(Query $query);
}