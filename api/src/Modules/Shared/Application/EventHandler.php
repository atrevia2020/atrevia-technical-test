<?php


namespace App\Modules\Shared\Application;


use Symfony\Component\Messenger\Handler\MessageSubscriberInterface;

interface EventHandler extends MessageSubscriberInterface
{
}