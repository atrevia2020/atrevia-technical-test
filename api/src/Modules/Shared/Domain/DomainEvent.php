<?php

namespace App\Modules\Shared\Domain;

use Ramsey\Uuid\Uuid;

abstract class DomainEvent implements Event
{
    private string $eventId;
    private \Datetime $occurredOn;

    private string $aggregateId;

    public function __construct(string $aggregateId)
    {
        $this->aggregateId = $aggregateId;

        $this->eventId    = Uuid::uuid4();
        $this->occurredOn = new \DateTime();
    }

    public function eventId(): string
    {
        return $this->eventId;
    }

    public function occurredOn(): \DateTime
    {
        return $this->occurredOn;
    }

    public function aggregateId(): string
    {
        return $this->aggregateId;
    }
}