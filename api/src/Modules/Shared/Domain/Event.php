<?php

namespace App\Modules\Shared\Domain;

interface Event
{
    public function eventId(): string;

    public function occurredOn(): \DateTime;
}