<?php


namespace App\Modules\Shared\Domain;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\MappedSuperclass;
use JMS\Serializer\Annotation as Serializer;

/** @MappedSuperclass */
abstract class AggregateRoot
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="string")
     */
    protected string $uuid;

    /** @Serializer\Exclude() */
    protected array $domainEvents = [];

    public function __construct(string $uuid)
    {
        $this->uuid = $uuid;
    }

    public function uuid(): string
    {
        return $this->uuid;
    }

    final public function getDomainEvents(): array
    {
        return $this->domainEvents;
    }

    final protected function record(DomainEvent $domainEvent): void
    {
        $this->domainEvents[] = $domainEvent;
    }
}
