<?php


namespace App\Modules\Shared\Infrastructure\Doctrine;

use App\Modules\Shared\Domain\AggregateRoot;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

abstract class DoctrineAbstractRepository extends ServiceEntityRepository
{
    public function persist($entity): void
    {
        $this->getEntityManager()->persist($entity);
    }

    public function remove($entity): void
    {
        $this->getEntityManager()->remove($entity);
    }

    public function findOrFail($id)
    {
        $entity = $this->find($id);

        if ($entity === null) {
            throw new NotFoundHttpException("{$this->_entityName} with id {$id} was not found");
        }

        return $entity;
    }

}
