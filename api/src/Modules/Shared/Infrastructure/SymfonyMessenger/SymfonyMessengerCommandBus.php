<?php


namespace App\Modules\Shared\Infrastructure\SymfonyMessenger;


use App\Modules\Shared\Application\Command;
use App\Modules\Shared\Application\CommandBus;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;

class SymfonyMessengerCommandBus implements CommandBus
{
    use HandleTrait {
        handle as private handleCommand;
    }

    public function __construct(
        MessageBusInterface $commandBus
    )
    {
        $this->messageBus = $commandBus;
    }

    public function handle(Command $command)
    {
        return $this->handleCommand($command);
    }
}