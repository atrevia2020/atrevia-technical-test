<?php


namespace App\Modules\Shared\Infrastructure\SymfonyMessenger;

use App\Modules\Shared\Application\EventBus;
use App\Modules\Shared\Domain\Event;
use Symfony\Component\Messenger\MessageBusInterface;

class SymfonyMessengerEventBus implements EventBus
{
    private static array $eventsDispatched = [];

    public function getLastEventDispatched(): Event
    {
        return static::$eventsDispatched[count(static::$eventsDispatched) - 1];
    }

    public function __construct(
        private MessageBusInterface $eventBus
    )
    {
    }

    public function dispatch(Event ...$events): void
    {
        foreach ($events as $event) {
            static::$eventsDispatched[] = $event;
            $this->eventBus->dispatch($event);
        }
    }

    public function getEventsDispatched(): array
    {
        return static::$eventsDispatched;
    }
}