<?php


namespace App\Modules\Shared\Infrastructure\SymfonyMessenger;


use App\Modules\Shared\Application\Query;
use App\Modules\Shared\Application\QueryBus;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;

class SymfonyMessengerQueryBus implements QueryBus
{
    use HandleTrait {
        handle as private handleQuery;
    }

    public function __construct(
        MessageBusInterface $queryBus
    )
    {
        $this->messageBus = $queryBus;
    }

    public function query(Query $query)
    {
        return $this->handleQuery($query);
    }
}