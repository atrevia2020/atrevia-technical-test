<?php


namespace App\Modules\Shared\Infrastructure;


use App\Modules\Shared\Domain\Event;
use Ramsey\Uuid\Uuid;

abstract class InfrastructureEvent implements Event
{
    private string $eventId;
    private \Datetime $occurredOn;

    public function __construct()
    {
        $this->eventId    = Uuid::uuid4();
        $this->occurredOn = new \DateTime();
    }

    public function eventId(): string
    {
        return $this->eventId;
    }
}