<?php


namespace App\Modules\Shared\Infrastructure;

use Ramsey\Uuid\Uuid;

final class RamseyUuidGenerator
{
    public function generate(): string
    {
        return Uuid::uuid4()->toString();
    }
}