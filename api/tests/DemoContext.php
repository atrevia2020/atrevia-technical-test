<?php

declare(strict_types=1);

namespace App\Tests;


use App\Modules\Shared\Infrastructure\SymfonyMessenger\SymfonyMessengerEventBus;
use Behat\Behat\Context\Context;
use Behatch\HttpCall\Request;
use Doctrine\ORM\EntityManagerInterface;

final class DemoContext implements Context
{

    public function __construct(
        private Request $request,
        private \App\Modules\Shared\Application\EventBus $eventBus
    )
    {
    }

    /**
     * @Given I send JSON
     */
    public function iSendJSON()
    {
        $this->request->setHttpHeader('Content-Type', 'application/json');
    }

    /**
     * @Then a :eventName should have been dispatched
     */
    public function aShouldHaveBeenDispatched(string $eventName)
    {
        $eventsNames = array_map(function ($event) {
            $eventName = explode('\\', get_class($event));
            return end($eventName);
        }, $this->eventBus->getEventsDispatched());

        $eventsNamesFiltered = array_filter($eventsNames, function ($en) use ($eventName) {
            return $en === $eventName;
        });

        if(count($eventsNamesFiltered) === 0){
            throw new \Exception($eventName . ' event was not dispatched');
        }
    }

}
