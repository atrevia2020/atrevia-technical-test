Feature:
    Create discount coupon

    Scenario: Fail when code exists
        Given I send JSON
        When I send a "POST" request to "/api/backoffice/v1/public/coupons/123e4567-e89b-12d3-a456-426655440000" with body:
        """
        {
            "code": "ATREVIA2022"
        }
        """
        Given I send JSON
        When  I send a "POST" request to "/api/backoffice/v1/public/coupons/123e4567-e89b-12d3-a456-426655440000" with body:
        """
        {
            "code": "ATREVIA2022"
        }
        """
        Then the response status code should be 409

    Scenario: Success when code does not exists
        Given I send JSON
        When I send a "POST" request to "/api/backoffice/v1/public/coupons/123e4567-e89b-12d3-a456-426655440000" with body:
        """
        {
            "code": "ATREVIA2022"
        }
        """
        Then the response status code should be 200
        And a "DiscountCouponCreatedEvent" should have been dispatched