Feature:
    Redeem discount coupon

    Scenario: Fail when code has been already redeemed
        Given I send JSON
        When I send a "POST" request to "/api/backoffice/v1/public/coupons/123e4567-e89b-12d3-a456-426655440000" with body:
        """
        {
            "code": "ATREVIA2022"
        }
        """
        Given I send JSON
        When  I send a "POST" request to "/api/backoffice/v1/public/coupons/123e4567-e89b-12d3-a456-426655440000/redeem" with body:
        """
        {
            "redeemerEmail": "acalvo@atrevia.com"
        }
        """
        Given I send JSON
        When  I send a "POST" request to "/api/backoffice/v1/public/coupons/123e4567-e89b-12d3-a456-426655440000/redeem" with body:
        """
        {
            "redeemerEmail": "acalvo@atrevia.com"
        }
        """
        Then the response status code should be 409

    Scenario: Fail when code has been already deactivated
        Given I send JSON
        When I send a "POST" request to "/api/backoffice/v1/public/coupons/123e4567-e89b-12d3-a456-426655440000" with body:
        """
        {
            "code": "ATREVIA2022"
        }
        """
        Then the response status code should be 200
        Given I send JSON
        When  I send a "POST" request to "/api/backoffice/v1/public/coupons/123e4567-e89b-12d3-a456-426655440000/deactivate" with body:
        """
        {
        }
        """
        Then the response status code should be 200
        Given I send JSON
        When  I send a "POST" request to "/api/backoffice/v1/public/coupons/123e4567-e89b-12d3-a456-426655440000/redeem" with body:
        """
        {
            "redeemerEmail": "acalvo@atrevia.com"
        }
        """
        Then the response status code should be 409

    Scenario: Success when discount coupon is available and active
        Given I send JSON
        When I send a "POST" request to "/api/backoffice/v1/public/coupons/123e4567-e89b-12d3-a456-426655440005" with body:
        """
        {
            "code": "ATREVIA2022"
        }
        """
        Given I send JSON
        When I send a "POST" request to "/api/backoffice/v1/public/coupons/123e4567-e89b-12d3-a456-426655440005/redeem" with body:
        """
        {
            "redeemerEmail": "acalvo@atrevia.com"
        }
        """
        Then the response status code should be 200
        And a "DiscountCouponRedeemedEvent" should have been dispatched