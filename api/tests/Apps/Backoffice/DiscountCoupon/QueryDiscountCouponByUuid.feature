Feature:
    Query discount coupon by uuid

    Scenario: Fail when discount does not exists
        Given I send JSON
        When  I send a "GET" request to "/api/backoffice/v1/public/coupons/123e4567-e89b-12d3-a456-426655440000"
        Then the response status code should be 404

    Scenario: Fail when discount does exists
        Given I send JSON
        When I send a "POST" request to "/api/backoffice/v1/public/coupons/123e4567-e89b-12d3-a456-426655440000" with body:
        """
        {
            "code": "ATREVIA2022"
        }
        """
        Given I send JSON
        When  I send a "GET" request to "/api/backoffice/v1/public/coupons/123e4567-e89b-12d3-a456-426655440000"
        Then the response status code should be 200

