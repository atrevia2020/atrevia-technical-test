Feature:
    Rename discount coupon

    Scenario: Success when discount coupon is available and active
        Given I send JSON
        When I send a "POST" request to "/api/backoffice/v1/public/coupons/123e4567-e89b-12d3-a456-426655440005" with body:
        """
        {
            "code": "ATREVIA2022"
        }
        """
        Given I send JSON
        When I send a "POST" request to "/api/backoffice/v1/public/coupons/123e4567-e89b-12d3-a456-426655440005/rename" with body:
        """
        {
            "code": "ATREVIA2023"
        }
        """
        Then the response status code should be 200
        And a "DiscountCouponRenamedEvent" should have been dispatched