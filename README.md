# Prueba técnica de ATREVIA

## Para Seniors

Haz un fork de este repositorio, trabaja sobre él.

### Primer ejercicio
Ahora nuestro negocio quiere dejar de ver en los listados algunos códigos. Quieren eliminarlos, pero no quieren que se pierda la información. Implementa de alguna forma un sistema para el borrado lógico de los descuentos. 

El borrado lógico debe permitir a los técnicos acceder a esos datos vía SQL, pero debe mostrar a los usuarios del backoffice solo los que no estén borrados, aunque pudiesen estar desactivados. 

Este cambio quieren que afecte no solo a los cupones de descuento, sino a todas las entidades que se añadan posteriormente al sistema. 

Crea un endpoint para consultar, como usuario, todos los códigos de descuentos que no hayan sido borrados.

En este ejercicio queremos **evaluar el conocimiento sobre Symfony y Doctrine**.

No olvides los tests.

### Segundo ejercicio

Ahora además de guardar la información del usuario que ha canjeado un código de descuento queremos que se **envíe un correo electrónico** al mismo donde se le notifique que se ha canjeado su código satisfactoriamente.

Implementa el caso de uso sin implementar el cliente de correo, adecúate a la interfaz de `App\Modules\Shared\Application\EmailSender`.

En este ejercicio queremos **evaluar el conocimiento sobre diseño y arquitectura de software**.

No es necesario hacer test para este caso.

## Para Juniors

Haz un fork de este repositorio, trabaja sobre él.

Ahora a nuestro negocio le interesa saber en qué tienda se han canjeado los códigos. Almacena en los códigos de descuentos un identificador de tienda (merchant) donde se indique.

Después, crea un nuevo endpoint que permita consultar todos aquellos códigos de descuento canjeados en una tienda en específico.

No olvides los tests.

## Instalación con Docker (recomendable)

Pasos a ejecutar desde la carpeta raíz del proyecto para la instalación:

- Instalar Docker (https://docs.docker.com/get-docker/) y Docker Compose (https://docs.docker.com/compose/install/)
- `sh docker/install.sh --with-hosts` (opcional para modificar el etc/hosts)
- `sh docker/command.sh` doctrine:migrations:migrate
- ¡Listo!

Para parar el sistema

- `sh docker/stop.sh`

Para lanzarlo

- `sh docker/start.sh`

Para usar composer

- `sh docker/composer.sh` (`require doctrine/annotations`)

Para ejecutar comando

- `sh docker/command.sh` (`doctrine:schema:update -f`)

Para ejecutar los tests del backend

- `sh docker/test.sh`

## Instalación en local

Instala las dependencias con `composer install` en la carpeta *api/*.

Crea la base de datos en tu local y sustituye en *api/.env.local* las variables de entorno que tengan que ver con la conexión a ella.

Lanza la aplicación con `symfony server:start` (previamente has de tener instalado Symfony CLI).

Consulta la documentación de Symfony (https://symfony.com/doc/current/index.html) si tienes dudas.

Para ejecutar los tests del backend tienes que ir a la carpeta *api/*

- `cd api`
- `php vendor/bin/behat`

## Estrategia de testing

Son test de integración hechos con Behat.

Las fixtures se resetean después de cada test, no puedes hacer un a test depender de otro test
